from django.conf.urls import url


from channels.routing import ProtocolTypeRouter,URLRouter
from channels.auth import AuthMiddlewareStack

from realtime_colaborative_whiteboard.whiteboard.consumers import WhiteBoardConsumer


application = ProtocolTypeRouter({
        # WebSocket strokes handler
        "websocket": AuthMiddlewareStack(
            URLRouter([
                url(r"^white-board/$", WhiteBoardConsumer),
            ])
        ),
})


