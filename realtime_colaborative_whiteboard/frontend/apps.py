from django.apps import AppConfig


class FrontendConfig(AppConfig):
    name = 'realtime_colaborative_whiteboard.frontend'
