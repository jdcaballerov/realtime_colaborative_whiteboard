import React from "react";
import ReactDOM from "react-dom";
import Immutable, { List } from "immutable"

function fromJSGreedy(js) {
    return typeof js !== 'object' || js === null ? js :
        Array.isArray(js) ?
            Immutable.Seq(js).map(fromJSGreedy).toList() :
            Immutable.Seq(js).map(fromJSGreedy).toMap();
}

class DrawArea extends React.Component {
    constructor() {
        super();

        this.state = {
            lines: new Immutable.List(),
            isDrawing: false,
            color: 'blue',
            // Websocket
            ws: null
        };

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);

        this.handleColorChange = this.handleColorChange.bind(this);
    }

    timeout = 250;

    componentDidMount() {
        document.addEventListener("mouseup", this.handleMouseUp);
        // single websocket instance for the application and constantly trying to reconnect.
        this.connect();

        //console.log(this.lineFromPath('M 64 340 L 66 340 L 77 340 L 91 339 L 105 339 L 118 338 L 149 338 L 168 338 L 179 338 L 183 338 L 185 338 L 186 338'));


    }

    componentWillUnmount() {
        document.removeEventListener("mouseup", this.handleMouseUp);
    }

    handleMouseDown(mouseEvent) {
        if (mouseEvent.button != 0) {
            return;
        }

        const point = this.relativeCoordinatesForEvent(mouseEvent);

        let newLine = {
            'color': 'red',
            'coordinates': [],
            'synced': false
        }
        newLine = fromJSGreedy(newLine);
        newLine = newLine.updateIn(['coordinates'], line => line.push(point));
        this.setState(prevState => ({
            lines: prevState.lines.push(new Immutable.Map(newLine)),
            isDrawing: true
        }));
    }

    handleMouseMove(mouseEvent) {
        if (!this.state.isDrawing) {
            return;
        }

        const point = this.relativeCoordinatesForEvent(mouseEvent);

        this.setState(prevState => ({
            lines: prevState.lines.updateIn([prevState.lines.lastIndexOf(), 'coordinates'], line => line.push(point))
        }));
    }

    handleMouseUp() {
        this.setState({ isDrawing: false }, () => this.sendStroke());
    }

    handleColorChange(event) {
        this.setState({ color: event.target.value }, () => console.log(this.state.color));
    }

    sendStroke() {
        const lastStroke = this.state.lines.get(this.state.lines.size - 1);

        if(lastStroke == undefined)
            return

        if (lastStroke.get('synced') == false) {
            const lastStrokeCoordinates = lastStroke.get('coordinates')
            const stroke = JSON.stringify(
                {
                    'type': 'create.stroke',
                    'data': {
                        'path': this.getPath(lastStrokeCoordinates),
                        'color': this.state.color
                    }
                }
            )
            const { ws } = this.state
            try {
                ws.send(stroke)
            } catch (error) {
                // Retry, save to local storage, revert, etc
                console.log(error)
            }
            finally {

                this.setState(prevState => ({
                    lines: prevState.lines.filterNot((line, idx) => {
                        return line.has('synced');
                    })
                }));
            }
        }
    }
    /** 
     * @function getPath
     * Converts an Immutable List of Immutable Maps of coordinates to an SVG Path like string
     * [{'x':12,'y':12}, {'x':20,'y':15}] -> "M 12 12 L 20 15"  
    */
    getPath(line) {
        const pathData = "M " +
            line
                .map(p => {
                    return `${p.get('x')} ${p.get('y')}`;
                })
                .join(" L ");

        return pathData;
    }
    /** 
     * @function lineFromPath
     * Converts an SVG Path like string to an Immutable List of Immutable Maps of coordinates 
     * "M 12 12 L 20 15" -> [{'x':12,'y':12}, {'x':20,'y':15}]
    */
    lineFromPath(path) {

        const commands = path.split(/(?=[LMC])/);
        const line = Immutable.List(
            commands.map(d => {
                // Slice skips command i.e M, L, C
                let pointsArray = d.slice(1, d.length).split(' ');
                let point = new Immutable.Map({
                    x: pointsArray[1],
                    y: pointsArray[2],
                });
                return point;
            })
        );
        return line;
    }

    relativeCoordinatesForEvent(mouseEvent) {
        const boundingRect = this.refs.drawArea.getBoundingClientRect();
        return new Immutable.Map({
            x: mouseEvent.clientX - boundingRect.left,
            y: mouseEvent.clientY - boundingRect.top,
        });
    }


    /**
     * @function connect
     * This function establishes the connection with the websocket and also ensures constant reconnection if connection closes
     */
    connect = () => {
        var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        var ws = new WebSocket(ws_scheme + '://' + window.location.host + '/white-board/');
        let that = this; // cache the this
        var connectInterval;

        // websocket onopen event listener
        ws.onopen = () => {
            console.log("connected websocket main component");

            this.setState({ ws: ws });

            that.timeout = 250; // reset timer to 250 on open of websocket connection 
            clearTimeout(connectInterval); // clear Interval  on open of websocket connection
        };

        ws.onmessage = event => {
            const message = JSON.parse(event.data);
            if (message.type == "create.stroke") {

                const linesFromBackend = Immutable.List(
                    message.data.map(stroke => new Immutable.Map.of('coordinates', this.lineFromPath(stroke.path),
                        'color', stroke.color == undefined ? 'black' : stroke.color, 'id', stroke.id))
                );
                //console.log(linesFromBackend);


                this.setState(prevState => ({
                    //lines: prevState.lines.concat(linesFromBackend)
                    lines: linesFromBackend.concat(prevState.lines)
                }));
            }
        }

        // websocket onclose event listener
        ws.onclose = event => {
            console.log(
                `Socket is closed. Reconnect will be attempted in ${Math.min(
                    10000 / 1000,
                    (that.timeout + that.timeout) / 1000
                )} second.`,
                event.reason
            );

            that.timeout = that.timeout + that.timeout; //increment retry interval
            connectInterval = setTimeout(this.check, Math.min(10000, that.timeout)); //call check function after timeout
        };

        // websocket onerror event listener
        ws.onerror = err => {
            console.error(
                "Socket encountered error: ",
                err.message,
                "Closing socket"
            );

            ws.close();
        };
    };

    /**
     * utility for the @function connect to check if the connection is closed, if so attempts to reconnect
     */
    check = () => {
        const { ws } = this.state;
        if (!ws || ws.readyState == WebSocket.CLOSED) this.connect(); //check if websocket instance is closed, if so call `connect` function.
    };


    render() {
        return (
            <div className="columns">
                <div className="column"
                    className="drawArea"
                    ref="drawArea"
                    onMouseDown={this.handleMouseDown}
                    onMouseMove={this.handleMouseMove}
                >
                    <Drawing lines={this.state.lines} />

                </div>
                <div className="column">
                    <ColorForm color={this.state.color} onChange={this.handleColorChange} />
                </div>
            </div>
        );
    }
}

function Drawing({ lines }) {
    return (
        <svg className="drawing">
            {lines.map((line, index) => (
                <DrawingLine key={index} line={line} />
            ))}
        </svg>
    );
}

function DrawingLine({ line }) {
    const pathData = "M " +
        line.get('coordinates')
            .map(p => {
                return `${p.get('x')} ${p.get('y')}`;
            })
            .join(" L ");

    return <path className="path" d={pathData} stroke={line.has('synced') ? 'red' : line.get('color')} />;
}

class ColorForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { color: this.props.color };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        //this.setState({ value: event.target.value });
        this.props.onChange(event)
    }

    render() {
        return (
            <label>
                Stroke's Color:
            <select value={this.props.color} onChange={this.handleChange}>
                    <option value="black">Black</option>
                    <option value="blue">Blue</option>
                    <option value="yellow">Yellow</option>
                    <option value="purple">Purple</option>
                </select>
            </label>
        );
    }
}




ReactDOM.render(<DrawArea />, document.getElementById("app"));
