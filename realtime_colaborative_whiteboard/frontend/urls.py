from django.contrib.auth.decorators import login_required
from django.urls import path

from . import views

app_name = "frontend"

urlpatterns = [
    path('', login_required(views.index)),
]