from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "realtime_colaborative_whiteboard.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import realtime_colaborative_whiteboard.users.signals  # noqa F401
        except ImportError:
            pass
