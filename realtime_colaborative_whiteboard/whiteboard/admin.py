from django.contrib import admin
from .models import Stroke

@admin.register(Stroke)
class StrokeAdmin(admin.ModelAdmin):
    pass