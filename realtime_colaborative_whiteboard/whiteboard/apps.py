from django.apps import AppConfig


class WhiteboardConfig(AppConfig):
    name = 'realtime_colaborative_whiteboard.whiteboard'
