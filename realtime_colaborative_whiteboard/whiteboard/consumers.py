from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.db import database_sync_to_async

from .serializers import ReadOnlyStrokeSerializer, StrokeSerializer
from .models import Stroke


class WhiteBoardConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        user = self.scope["user"]
        if user.is_anonymous:
            await self.close()
        else:
            await self.accept()
            self.channel_layer.group_add("board", self.channel_name)
            strokes = await self._get_strokes(self.scope['user'])

            strokes_data = [ReadOnlyStrokeSerializer(stroke).data for stroke in strokes]
            await self.send_json({
                'type': 'create.stroke',
                'data': strokes_data
            })

    async def disconnect(self, close_code):
        self.channel_layer.group_discard("board", self.channel_name)

    async def receive_json(self, content, **kwargs):
        message_type = content.get('type')
        if message_type == 'create.stroke':
            await self.send_stroke(content)

    async def send_stroke(self,event):
        stroke = await self._create_stroke(event.get('data'))
        stroke_data = ReadOnlyStrokeSerializer(stroke).data
       # await self.send_json({
       #     'type': 'create.stroke',
       #     'data': [stroke_data]
       # })

        await self.channel_layer.group_send(
            "board",
            {
                "type": "create.stroke",
                "data": [stroke_data],
            }
        )

    async def create_stroke(self, event):
        await self.send_json(
        {
            'type': 'create.stroke',
            'data': event['data']
        },
    )

    @database_sync_to_async
    def _create_stroke(self, content):
        # Add user id
        content['user'] = self.scope["user"].pk

        serializer = StrokeSerializer(data=content)
        serializer.is_valid(raise_exception=True)
        stroke = serializer.create(serializer.validated_data)
        return stroke

    @database_sync_to_async
    def _get_strokes(self, user):
        if not user.is_authenticated:
            raise Exception('User is not authenticated.')
        #TODO Filter visible
        return Stroke.objects.all().order_by('created')