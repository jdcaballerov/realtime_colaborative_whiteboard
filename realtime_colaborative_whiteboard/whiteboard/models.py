from django.db import models
from django.conf import settings

import uuid



class Stroke(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        null=True,
        blank=True,
        on_delete=models.DO_NOTHING,
        related_name='strokes'
    )

    # path
    path = models.TextField(blank=False)
    #visible = models.BooleanField(default=True)
    color = models.CharField(default='black', max_length=255)

    def __str__(self):
        return f'{self.id}'