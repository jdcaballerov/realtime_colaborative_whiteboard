from django.contrib.auth import get_user_model

from rest_framework import serializers

from .models import Stroke

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = (
            'id', 'username',
            )
        read_only_fields = ('id',)

class StrokeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stroke
        fields = '__all__'
        read_only_fields = ('id','created','updated')

class ReadOnlyStrokeSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Stroke
        fields = '__all__'