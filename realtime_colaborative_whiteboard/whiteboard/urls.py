from django.contrib.auth.decorators import login_required
from django.urls import path

from rest_framework.generics import ListCreateAPIView

from .models import Stroke
from .serializers import ReadOnlyStrokeSerializer

app_name = "whiteboard"

urlpatterns = [
    path('download', login_required(ListCreateAPIView.as_view(queryset=Stroke.objects.all(), serializer_class=ReadOnlyStrokeSerializer)),name='download'),
]